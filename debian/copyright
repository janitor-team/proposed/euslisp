Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: EusLisp
Upstream-Contact: Toshihiro MATSUI
Source: https://github.com/euslisp/EusLisp
Files-Excluded: contrib/glut/* doc/html/* doc/mails/* doc/licence-old/* doc/publications/* doc/rsj-euslisp1997.pdf doc/sigsym-paper.txt doc/islisp.ps lib/demo/gosper.l lib/llib/loop.l
Comment: Supplied GL/GLU/GLX/GLUT files, Non-free SGI License
           (lisp/opengl/src/{gl,glu,glx}const.l, contrib/glut/*)
	 Supplied gosper demo files, unknown license (SGI)
	   (lib/demo/gosper.l)
	 Supplied loop.l files, unknown license (MIT)
	   (lib/llib/loop.l)
	 Supplied publications, documents, unknown license
	   (doc/html/* ,doc/mails/*, doc/licence-old/*, doc/publications/*, doc/rsj-euslisp1997.pdf, doc/sigsym-paper.txt, doc/islisp.ps)

Files: *
Copyright: 1984-2001, Toshihiro MATSUI, Electrotechnical Laboratory
	   1984, Taiichi Yuasa and Masami Hagiya
	   1993, Isao HARA, Electrotechnical Laboratory
	   2001, Masahiro YASUGI
	   1994, Hirofumi NAKAGAKI, KEPCO
	   2001-2003, Toshihiro MATSUI, National Institute of Advanced Industrial Science and Technology (AIST)
	   2018, Toshihiro MATSUI, Institute of Information Security (IISEC)
License: BSD-3-clause

Files:     lisp/tool/eustags.c
Copyright: 1984-1988 Free Software Foundation, Inc. and Ken Arnold
License: GENERAL-PUBLIC-LICENSE-TO-COPY

Files:     contrib/ops5/*
Copyright: 1979, 1980, 1981, harles L. Forgy,  Pittsburgh, Pennsylvania
License: public-domain-OPS5

Files:     lisp/opengl/src/glconst.l
Copyright: 1999-2006  Brian Paul
	   2009  VMware, Inc
License: MIT

Files:     lisp/opengl/src/gluconst.l
Copyright: 1991-2000 Silicon Graphics, Inc.
License: SGI-B-2

Files:     lisp/opengl/src/glxconst.l
Copyright: 1999-2006  Brian Paul
License: MIT

Files: debian/*
Copyright: 2019, Kei Okada <kei.okada@gmail.com>
License: BSD-3-clause

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 * Neither the name of the National Institute of Advanced Industrial Science
   and Technology (AIST) nor the names of its contributors may be used to
   endorse or promote products derived from this software without specific prior
   written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

License: SGI-B-2
 SGI FREE SOFTWARE LICENSE B (Version 2.0, Sept. 18, 2008)
 .
 Copyright (C) 1991-2000 Silicon Graphics, Inc. All Rights Reserved.
 .
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is furnished to do
 so, subject to the following conditions:
 .
 The above copyright notice including the dates of first publication and either
 this permission notice or a reference to http://oss.sgi.com/projects/FreeB/
 shall be included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL SILICON GRAPHICS, INC. BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 IN THE SOFTWARE.
 .
 Except as contained in this notice, the name of Silicon Graphics, Inc. shall
 not be used in advertising or otherwise to promote the sale, use or other
 dealings in this Software without prior written authorization from Silicon
 Graphics, Inc.

License: GENERAL-PUBLIC-LICENSE-TO-COPY
 NO WARRANTY
 .
 BECAUSE THIS PROGRAM IS LICENSED FREE OF CHARGE, WE PROVIDE ABSOLUTELY
 NO WARRANTY, TO THE EXTENT PERMITTED BY APPLICABLE STATE LAW.  EXCEPT
 WHEN OTHERWISE STATED IN WRITING, FREE SOFTWARE FOUNDATION, INC,
 RICHARD M. STALLMAN AND/OR OTHER PARTIES PROVIDE THIS PROGRAM "AS IS"
 WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY
 AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE PROGRAM PROVE
 DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR
 CORRECTION.
 .
 IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW WILL RICHARD M.
 STALLMAN, THE FREE SOFTWARE FOUNDATION, INC., AND/OR ANY OTHER PARTY
 WHO MAY MODIFY AND REDISTRIBUTE THIS PROGRAM AS PERMITTED BELOW, BE
 LIABLE TO YOU FOR DAMAGES, INCLUDING ANY LOST PROFITS, LOST MONIES, OR
 OTHER SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE
 USE OR INABILITY TO USE (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR
 DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY THIRD PARTIES OR
 A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS) THIS
 PROGRAM, EVEN IF YOU HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH
 DAMAGES, OR FOR ANY CLAIM BY ANY OTHER PARTY.
 .
 GENERAL PUBLIC LICENSE TO COPY
 .
 1. You may copy and distribute verbatim copies of this source file
 as you receive it, in any medium, provided that you conspicuously
 and appropriately publish on each copy a valid copyright notice
 "Copyright (C) 1986 Free Software Foundation"; and include
 following the copyright notice a verbatim copy of the above disclaimer
 of warranty and of this License.
 .
 2. You may modify your copy or copies of this source file or
 any portion of it, and copy and distribute such modifications under
 the terms of Paragraph 1 above, provided that you also do the following:
 .
 a) cause the modified files to carry prominent notices stating
 that you changed the files and the date of any change; and
 .
 b) cause the whole of any work that you distribute or publish,
 that in whole or in part contains or is a derivative of this
 program or any part thereof, to be licensed at no charge to all
 third parties on terms identical to those contained in this
 License Agreement (except that you may choose to grant more extensive
 warranty protection to some or all third parties, at your option).
 .
 c) You may charge a distribution fee for the physical act of
 transferring a copy, and you may at your option offer warranty
 protection in exchange for a fee.
 .
 Mere aggregation of another unrelated program with this program (or its
 derivative) on a volume of a storage or distribution medium does not bring
 the other program under the scope of these terms.
 .
 3. You may copy and distribute this program (or a portion or derivative
 of it, under Paragraph 2) in object code or executable form under the terms
 of Paragraphs 1 and 2 above provided that you also do one of the following:
 .
 a) accompany it with the complete corresponding machine-readable
 source code, which must be distributed under the terms of
 Paragraphs 1 and 2 above; or,
 .
 b) accompany it with a written offer, valid for at least three
 years, to give any third party free (except for a nominal
 shipping charge) a complete machine-readable copy of the
 corresponding source code, to be distributed under the terms of
 Paragraphs 1 and 2 above; or,
 .
 c) accompany it with the information you received as to where the
 corresponding source code may be obtained.  (This alternative is
 allowed only for noncommercial distribution and only if you
 received the program in object code or executable form alone.)
 .
 For an executable file, complete source code means all the source code for
 all modules it contains; but, as a special exception, it need not include
 source code for modules which are standard libraries that accompany the
 operating system on which the executable file runs.
 .
 4. You may not copy, sublicense, distribute or transfer this program
 except as expressly provided under this License Agreement.  Any attempt
 otherwise to copy, sublicense, distribute or transfer this program is void and
 your rights to use the program under this License agreement shall be
 automatically terminated.  However, parties who have received computer
 software programs from you with this License Agreement will not have
 their licenses terminated so long as such parties remain in full compliance.
 .
 In other words, you are welcome to use, share and improve this program.
 You are forbidden to forbid anyone else to use, share and improve
 what you give them.   Help stamp out software-hoarding!

License: public-domain-OPS5
 This Common Lisp version of OPS5 is in the public domain.  It is based
 in part on based on a Franz Lisp implementation done by Charles L. Forgy
 at Carnegie-Mellon University, which was placed in the public domain by
 the author in accordance with CMU policies.  This version has been
 modified by George Wood, Dario Giuse, Skef Wholey, Michael Parzen,
 and Dan Kuokka.
